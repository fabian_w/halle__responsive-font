let word = ''
 document.addEventListener('keydown', (event) => {
   if (event.key.match(/[a-zA-Z]/g).length === 1) {
     word += event.key
   }
   if (word.length === 6 || event.keyCode === 13) {
     const letters = document.querySelectorAll('.letter')
     for (var j = 0; j < letters.length; j++) {
       letters[j].classList.remove(...letters[j].classList)
       letters[j].classList.add('letter')
     }
     for (var i = 0; i < word.length; i++) {
       const letter = word.charAt(i)
       const element = letters[i]
       element.classList.add('letter', letter)
     }
     word = ''
   }
 })
